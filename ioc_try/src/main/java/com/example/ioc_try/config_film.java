package com.example.ioc_try;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class config_film {

	@Bean
	public Film film1() {
		Film film1 = new Film();
		
		film1.setName("Journey");
		film1.setPrice(1000.00);
		return film1;
	}
	
	@Bean
	public Books book1() {
		Books book1 = new Books();
		
		book1.setName("Eragon");
		book1.setPrice(1000.00);
		return book1;
	}
}
