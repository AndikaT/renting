package com.example.ioc_try;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.example.ioc_try.Books;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class IocTryApplication {

	public static void main(String[] args) {
		//SpringApplication.run(IocTryApplication.class, args);
		
		//ApplicationContext rootctx = new ClassPathXmlApplicationContext("springContext.xml");
		
		AnnotationConfigApplicationContext rootctx = new AnnotationConfigApplicationContext(config_renter.class);
		
		Renter obj1 = (Renter)rootctx.getBean("rent_film");
		
		rootctx.close();
		System.out.println(obj1.getProduct().get_type());
		System.out.println(obj1.getProduct().getName());
		System.out.println(obj1.getProduct().getPrice());
		
		
		//stream stuff
		List<Books> book = new ArrayList<Books>();
		
		book.add(new Books("Eragon",30000.00));
		book.add(new Books("Eragon",30000.00));
		book.add(new Books("Eragon2",40000.00));
		book.add(new Books("Eragon3",50000.00));
		book.add(new Books("Eragon4",20000.00));
		book.add(new Books("Eragon5",10000.00));
		
		//book.stream().filter(b->b.price >20000).forEach(b->System.out.println(b.name));
		
		//List<Books> sorted_book = book.stream()
		//		.sorted(Comparator.comparingDouble(Books::getPrice))
		//		.collect(Collectors.toList());
		
		//sorted_book.stream().forEach(b->System.out.println(b.name));;
		
		//sorted_book = book.stream()
		//		.sorted(Comparator.comparingDouble(Books::getPrice).reversed())
		//		.collect(Collectors.toList());
		
		//sorted_book.stream().forEach(b->System.out.println(b.name));;
		
		//book.stream().distinct().forEach(b->System.out.println(b.name));
		
		List<String> book_name = book.stream().map(b->b.name).collect(Collectors.toList());
		
		book_name.stream().forEach(b->System.out.println(b));
		
	}

}
