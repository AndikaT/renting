package com.example.ioc_try;

public class Film implements Products {
	
	String name;
	Double price;

	@Override
	public String get_type() {
		// TODO Auto-generated method stub
		return "Films";
	}
	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

}
