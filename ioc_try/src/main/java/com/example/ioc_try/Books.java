package com.example.ioc_try;

public class Books implements Products {
	
	String name;
	Double price;
	
	public Books(String name, Double price) {
		this.name = name;
		this.price = price;
	}
	
	public Books() {
		
	}

	@Override
	public String get_type() {
		// TODO Auto-generated method stub
		return "books";
	}
	@Override
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	@Override
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	
}
