package com.rent.models;

public class Renter {
	
	String name;
	Products product;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Products getProduct() {
		return product;
	}
	public void setProduct(Products product) {
		this.product = product;
	}

}
