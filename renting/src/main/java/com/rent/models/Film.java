package com.rent.models;

public class Film implements Products {
	
	String name;
	Float price;

	@Override
	public String get_type() {
		// TODO Auto-generated method stub
		return "Films";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

}
