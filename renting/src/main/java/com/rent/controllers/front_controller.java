package com.rent.controllers;


import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.rent.models.Renter;

@Controller
@RequestMapping("/front")
public class front_controller {
	
	@GetMapping(value="index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("/front/index");
		ApplicationContext rootctx = new ClassPathXmlApplicationContext("springContext.xml");
		
		Renter obj1 = (Renter)rootctx.getBean("inject_set");
		
		view.addObject(obj1);
		return view;
	}

}
